delete from category;
delete from restaurant;
delete from city;
delete from pais;

INSERT INTO category(id, name) VALUES (1000, 'Desayuno');
INSERT INTO category(id, name) VALUES (1001, 'Almuerzo');
INSERT INTO category(id, name) VALUES (1002, 'Cena Familiar');

INSERT INTO city(id, name,pais_id) VALUES (1000, 'Cochabamba',1000);
INSERT INTO city(id, name,pais_id) VALUES (1001, 'La Paz',1000);
INSERT INTO city(id, name,pais_id) VALUES (1002, 'Sucre',1001);
INSERT INTO city(id, name,pais_id) VALUES (1003, 'Buenos aires',1001);

INSERT INTO pais(id, name) VALUES (1000, 'Bolivia');
INSERT INTO pais(id, name) VALUES (1001, 'Argentina');



INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1001, 'HyperMaxi',1000,1000,'buffet','foto1','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1002, 'IceNorte',1001,1001,'almuerzos','foto2','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1003, 'Planchitas',1002,1002,'platos variados','foto3','cbba');