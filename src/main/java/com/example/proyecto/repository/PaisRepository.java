package com.example.proyecto.repository;

import com.example.proyecto.entities.Pais;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;
@Transactional
public interface PaisRepository extends CrudRepository<Pais,Integer>{

}
