package com.example.proyecto.repository;

import com.example.proyecto.entities.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByUsername(String username);
    //User findByUserId(int userid);

}
