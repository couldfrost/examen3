package com.example.proyecto.services;

import com.example.proyecto.entities.Pais;
import com.example.proyecto.repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PaisServiceImpl implements PaisService {
    private PaisRepository paisRepository;

    @Autowired
    @Qualifier(value="paisRepository")
    public void setPaisRepository(PaisRepository paisRepository){
        this.paisRepository=paisRepository;
    }

    @Override
    public Iterable<Pais> listAllPaiss(){
        return  paisRepository.findAll();
    }
    @Override
    public void savePais(Pais pais){
        paisRepository.save(pais);
    }

    @Override
    public Pais getPais(Integer id){
        return paisRepository.findById(id).get();
    }
    @Override
    public void deletePais(Integer id){
        paisRepository.deleteById(id);;
    }

}
