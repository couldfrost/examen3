
package com.example.proyecto.services;

import com.example.proyecto.entities.Like;

public interface LikeService {
    Iterable<Like> listAllLikes();

    void saveLike(Like like);

    Like getLike(Integer id);

    void deleteLike(Integer id);
}
