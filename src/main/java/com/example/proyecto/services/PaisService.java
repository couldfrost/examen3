package com.example.proyecto.services;

import com.example.proyecto.entities.Pais;

public interface PaisService {

    Iterable<Pais> listAllPaiss();

    void savePais(Pais pais);

    Pais getPais(Integer id);

    void deletePais(Integer id);
}
