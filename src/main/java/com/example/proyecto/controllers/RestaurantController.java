package com.example.proyecto.controllers;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.entities.Category;
import com.example.proyecto.entities.City;
import com.example.proyecto.entities.Comment;
import com.example.proyecto.entities.Like;
import com.example.proyecto.entities.Pais;
import com.example.proyecto.services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RestaurantController {
    private RestaurantService restaurantService;
    private CategoryService categoryService;
    private CityService cityService;
    private LikeService likeService;
    private CommentService commentService;
    private PaisService paisService;

    @Autowired
    public void setRestaurantService(RestaurantService productService){
     this.restaurantService=productService;
    }
    @Autowired
    public void setCategoryService(CategoryService categoryService){
        this.categoryService=categoryService;
    }
    @Autowired
    public void setCityService(CityService cityService){
        this.cityService=cityService;
    }
    @Autowired
    public void setCommentService(CommentService commentService){
        this.commentService=commentService;
    }

    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
    public String list(Model model) {
        Iterable<Restaurant> restaurantList=restaurantService.listAllRestaurants();
                model.addAttribute("variableTexto","Hello world");
                model.addAttribute("restaurantList",restaurantList);
                return "restaurants";
    }

    @RequestMapping("/newRestaurant")
    String newRestaurant(Model model) {
        Iterable<Category> categories= categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities= cityService.listAllCitys();
        model.addAttribute("cities",cities);

                return "newRestaurant";
    }

    @RequestMapping(value="/restaurant", method = RequestMethod.POST)
    String save(Restaurant restaurant) {
        restaurantService.saveRestaurant(restaurant);
        return "redirect:/restaurants";
    }

    @RequestMapping("/restaurant/{id}")
    String show(@PathVariable Integer id,Model model) {
        Restaurant restaurant=restaurantService.getRestaurant(id);
        model.addAttribute("restaurant",restaurant);

        return "show";
    }

    @RequestMapping("/editRestaurant/{id}")
    String editRestaurant(@PathVariable Integer id,Model model) {

        Restaurant restaurant=restaurantService.getRestaurant(id);
        model.addAttribute("restaurant",restaurant);
        Iterable<Category> categories=categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);

        return "editRestaurant";
    }

    @RequestMapping("/deleteRestaurant/{id}")
    String delete(@PathVariable Integer id)
    {
        restaurantService.deleteRestaurant(id);
               return "redirect:/restaurants";
    }
     /*
     @RequestMapping("/like/{id}")
     String like(@PathVariable Integer id) {
                Restaurant restaurant = restaurantService.getRestaurant(id);
                Like like=likeService.getLike(id);
                //like.setLikes(like.getLikes()+1);

                restaurant.getLike().setLikes(restaurant.getLike().getLikes()+1);
                //like.getLikes();
                restaurantService.saveRestaurant(restaurant);
                likeService.saveLike(like);
                return "redirect:/restaurant/"+restaurant.getId();
    }
    */
    /*
    @RequestMapping("/valorar/{id}")
    String valorar(@PathVariable Integer id) {
        Restaurant restaurant = restaurantService.getRestaurant(id);
        Comment comment=commentService.getComment(id);

        restaurant.getComments()
        like.setLikes(like.getLikes()+1);

        restaurant.getLike().setLikes(restaurant.getLike().getLikes()+1);
        like.getLikes();
        restaurantService.saveRestaurant(restaurant);
        likeService.saveLike(like);
        return "redirect:/restaurant/"+restaurant.getId();
    }
*/

}
