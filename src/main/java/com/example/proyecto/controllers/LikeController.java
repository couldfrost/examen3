package com.example.proyecto.controllers;

import com.example.proyecto.entities.Like;
import com.example.proyecto.entities.User;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.services.LikeService;
import com.example.proyecto.services.LikeServiceImpl;
import com.example.proyecto.services.RestaurantService;
import com.example.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LikeController {

    private LikeService likeService;

    @Autowired
    public void setLikeService(LikeService likeService) {
        this.likeService = likeService;
    }

    @RequestMapping(value = "/like", method = RequestMethod.POST)
    String save(Like like) {
        likeService.saveLike(like);


        return "redirect:/restaurant/"+like.getRestaurant().getId();
    }

}
