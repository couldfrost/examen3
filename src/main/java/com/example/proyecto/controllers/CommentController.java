package com.example.proyecto.controllers;
import com.example.proyecto.entities.Comment;
import com.example.proyecto.entities.User;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.services.CommentService;
import com.example.proyecto.services.CommentServiceImpl;
import com.example.proyecto.services.RestaurantService;
import com.example.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {
        private CommentService commentService;
        private UserService userService;

        @Autowired
        public void setCommentService(CommentService commentService) {
                this.commentService = commentService;
        }

        @RequestMapping(value = "/comment", method = RequestMethod.POST)
        String save(Comment comment) {

                //User u = userService.findByUsername(auth.MetodoRetornaUsuarioLogeado.getUserName());
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                User u = (User) auth.getPrincipal();
                comment.setUser(u);
                com.example.proyecto.entities.User user = userService.findByUsername(u.getUsername());
                commentService.saveComment(comment);


                return "redirect:/restaurant/"+comment.getRestaurant().getId();
            }


}
